<?php
class Model_main extends Model{
    public function get_data() {	
        return ChatDB::getInstance()->getMessages();
    }
    
    public function insert_data($obj){
        return ChatDB::getInstance()->insertMessage($obj);
    }
    public function get_user_messages ($obj){
        return ChatDB::getInstance()->getUser($obj);
    }
    
    public function get_users(){
        return ChatDB::getInstance()->getUsers();
    }
}

class ChatDB{
    
    private static $connect = null;
    private static $instance = null;
    
    private function __construct() {
        try {
            
            self::$connect = new MongoClient();
            
        } catch (MongoConnectionException $ex) {
            die("������ ����������� � �������!");
        }
        catch (Exception $ex) {
            die($ex->getMessage());
        }
    }
    
    public static function getInstance() {
       if (self::$instance === null) {
         self::$instance = new self();
       }
       return self::$instance;
     }
     
    private function __clone() {
    }

    private function __wakeup() {
    }
    
    private function DateTimeForDB($date) {
        return $date->format("Y-m-d H:i:s");
    }
    
    public function getMessages() {
        try {
            $db = self::$connect->Chat;
            $collection = $db->messages;
            $result = $collection->find();
            return $result->sort(['date' => -1]);
        }
        catch (Exception $ex) {
            return false;
        }
    }
    
    public function insertMessage($data) {
        try {
            $message = ['user' => mb_convert_encoding($data['user'], "UTF-8", "cp1251"), 'message' => mb_convert_encoding($data['message'], "UTF-8", "cp1251"), 'date' => $this->DateTimeForDB(new DateTime())];
            $db = self::$connect->Chat;
            $collection = $db->messages;
            $collection->insert($message);
            return true;
        }
        catch (Exception $ex) {
            return false;
        }
    }
    
    public function getUser($data) {
        try {
            $db = self::$connect->Chat;
            $collection = $db->messages;
            $result = $collection->find(['user' =>  new MongoRegex("/" . mb_convert_encoding(mb_convert_encoding(rawurldecode($data), "cp1251", "UTF-8" ), "UTF-8", "cp1251") . "/i") ]);
            return $result->sort(['date' => -1]);
        }
        catch (Exception $ex) {
            return false;
        }
    }
    
    public function getUsers() {
        try {
            $db = self::$connect->Chat;
            $collection = $db->messages;
            $collection = $collection->distinct('user');
            return $collection;
        }
        catch (Exception $ex) {
            return false;
        }
    }
}