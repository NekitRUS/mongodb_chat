<?php
Class Controller_main extends Controller{
    function __construct() {
        $this->model = new Model_main();
        $this->view = new View();
    }
    
    function action_index() {
        
        $post = $_SERVER["REQUEST_METHOD"] == "POST";
        $error = array();
        $usersData = array();
        
        if ($post) {
            $userIsEmpty = false;
            $messageIsEmpty = false;
            
            $usersData = ['user' => trim($_POST['user']), 'message' => trim($_POST['message'])];
            
            if (empty($usersData['user'])) {
                $userIsEmpty = true;
                $error['user'] = true;
            }
            if (empty($usersData['message'])) {
                $messageIsEmpty = true;
                $error['message'] = true;
            }
            
            if (!$userIsEmpty && !$messageIsEmpty) {
                $this->model->insert_data($usersData);
                $usersData = array();
            }
        }
        
        $data['error'] = $error;
        $data['input'] = $usersData;
        $data['info']['messages'] = $this->model->get_data();
        $data['info']['users'] = $this->model->get_users();
        $this->view->generate('main_view.php', 'template_view.php', $data);
    }
    
    function action_profile($user) {
        
        $data['info']['messages'] = $this->model->get_user_messages($user);
        $data['info']['userName'] = mb_convert_encoding(rawurldecode($user), "cp1251", "UTF-8" );
        $this->view->generate('profile_view.php', 'template_view.php', $data);
    }
    
}